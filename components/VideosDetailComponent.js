import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Text, View, FlatList, StyleSheet } from 'react-native';
import { Button, Card, Tile } from 'react-native-elements';
import { fetchSeasons, fetchEpisodes } from '../redux/ActionCreator';

const mapStateToProps = (state) => {
	return {
		shows: state.shows,
		seasons: state.seasons,
		episodes: state.episodes
	};
};
const mapDispatchToProps = (dispatch) => ({
	fetchSeasons: (showId) => dispatch(fetchSeasons(showId)),
	fetchEpisodes: (seasonId, page, append) => dispatch(fetchEpisodes(seasonId, page, append))
});

class VideosDetail extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showId: this.props.navigation.getParam('showId', ''),
			selectedSeason: null
		};
	}
	componentDidUpdate(prevProps, prevState) {
		if (prevProps.seasons.seasons !== this.props.seasons.seasons) {
			const selectedSeason =
				this.props.seasons.seasons && this.props.seasons.seasons[0] && this.props.seasons.seasons[0];
			this.setState({ selectedSeason });
			this.getEpisodes(selectedSeason.id);
		}
	}
	componentDidMount() {
		this.props.fetchSeasons(this.state.showId);
	}
	getEpisodes = (seasond) => {
		this.props.fetchEpisodes(seasond, '');
	};
	changeSeason = (season) => {
		this.getEpisodes(season.id);
		this.setState({ selectedSeason: season });
	};
	handleRefresh = () => {
		this.setState(
			{
				refreshing: this.props.episodes.refreshing
			},
			() => this.props.fetchEpisodes(this.props.seasons.seasons[0].id, '')
		);
	};
	loadMore = () => {
		if (!this.props.episodes.isLastpage && this.props.episodes.nextPage !== undefined) {
			this.props.fetchEpisodes(this.props.seasons.seasons[0].id, this.props.episodes.nextPage, true);
		}
	};
	render() {
		const { navigate } = this.props.navigation;
		const renderEpisodes = ({ item, index }) => {
			const episode = { ...item, ...item.thumbnailImages[0] };
			return (
				<Card key={index} containerStyle={styles.cardContainer}>
					<Tile
						key={index}
						containerStyle={styles.tileContainer}
						imageSrc={{ uri: episode.image.src }}
						imageContainerStyle={styles.imageContainer}
						icon={{ name: 'youtube-play', type: 'font-awesome', size: 40, color: 'red' }}
						iconContainerStyle={{ justifyContent: 'center' }}
						title={(value = episode.title)}
						titleStyle={styles.title}
						onPress={() =>
							navigate('RenderVideo', {
								video: this.props.episodes.episodes.find(
									(ep) => ep.id.toString() === item.id.toString()
								)
							})}
					/>
				</Card>
			);
		};
		if (this.props.seasons.seasons.length || this.props.episodes.refreshing) {
			return (
				<React.Fragment>
					<View
						style={{
							flexDirection: 'row',
							justifyContent: 'center',
							margin: 20
						}}>
						<Text style={{ fontSize: 30 }}>Seasons </Text>

						<View style={{ flexDirection: 'row' }}>
							{this.props.seasons.seasons.map((season) => {
								return (
									<Button
										buttonStyle={{
											borderRadius: 30,
											width: 40,
											margin: 2,
											borderColor: 'black',
											backgroundColor: this.state.selectedSeason === season ? '#4682b4' : 'gray'
										}}
										titleStyle={{ fontWeight: '900' }}
										key={season.id}
										title={season.name}
										onPress={() => {
											this.changeSeason(season);
										}}
									/>
								);
							})}
						</View>
					</View>
					<Text style={{ fontSize: 20, textAlign: 'center', marginBottom: 15 }}>
						{this.state.selectedSeason ? this.state.selectedSeason.episodesCount + ' Episodes' : null}
					</Text>
					<FlatList
						data={this.props.episodes.episodes}
						renderItem={renderEpisodes}
						keyExtractor={(item, i) => {
							return item.id.toString() + i;
						}}
						onRefresh={this.handleRefresh}
						refreshing={this.props.episodes.refreshing}
						onEndReachedThreshold={0}
						onEndReached={this.loadMore}
					/>
				</React.Fragment>
			);
		} else {
			return <Text style={{ fontSize: 30, textAlign: 'center', paddingTop: 350 }}>No Seasons Found.</Text>;
		}
	}
}
const styles = StyleSheet.create({
	cardContainer: {
		flex: 1,
		maxHeight: 100,
		paddingTop: 0,
		paddingHorizontal: 0,
		marginHorizontal: 0,
		marginVertical: 0
	},
	tileContainer: {
		marginHorizontal: 0,
		marginVertical: 0,
		flexDirection: 'row',
		paddingLeft: 0,
		position: 'relative'
	},
	imageContainer: {
		height: 100,
		maxWidth: 180
	},
	title: {
		maxWidth: 200,
		maxHeight: 200,
		fontSize: 15
	}
});
export default connect(mapStateToProps, mapDispatchToProps)(VideosDetail);
