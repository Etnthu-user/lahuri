import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchSearchResults, clearSearch, fetchTags, searchLoading } from '../redux/ActionCreator';
import { View, FlatList, TextInput, StyleSheet, Keyboard, Text } from 'react-native';
import { Icon, Card, Tile, ListItem } from 'react-native-elements';
import moment from 'moment/min/moment-with-locales';
moment.locale('ne');
import * as Animatable from 'react-native-animatable';

const mapStateToProp = (state) => {
	return {
		search: state.search,
		tags: state.tags
	};
};
const mapDispatchToProps = (dispatch) => ({
	searchLoading: () => dispatch(searchLoading()),
	fetchSearchResults: (statusId, searchQuery) => dispatch(fetchSearchResults(statusId, searchQuery)),
	clearSearchResults: () => dispatch(clearSearch()),
	fetchTags: () => dispatch(fetchTags())
});

class Search extends Component {
	constructor(props) {
		super(props);
		this.state = {
			searchTerm: '',
			statusId: 2,
			loading: this.props.search.isLoading
		};
	}
	static navigationOptions = () => {
		return {
			title: 'Search'
		};
	};
	onChangeText = (searchTerm) => {
		this.setState({ searchTerm });
		this.onSearchTermChange();
		// setTimeout(() => {
		// 	this.onSearchTermChange();
		// }, 0);
	};
	onSearchTermChange = () => {
		this.props.fetchSearchResults(this.state.statusId, this.state.searchTerm);
	};
	componentWillUnmount() {
		this.props.clearSearchResults();
	}
	componentDidMount() {
		this.props.fetchTags();
		this.textInputRef.focus();
	}
	render() {
		const renderTags = ({ item }) => {
			const tag = { ...item };
			return (
				<Animatable.View animation="slideInUp" duration={500}>
					<ListItem
						titleStyle={{ textAlign: 'center' }}
						onPress={() => navigate('PopularTags', { tagName: tag.tagName })}
						title={tag.tagName}
					/>
				</Animatable.View>
			);
		};
		const { navigate } = this.props.navigation;
		const renderSearch = ({ item, index }) => {
			const post = { ...item, ...item.postContents[0], ...item.thumbnailImages[0] };
			const date = post.publishedDate;
			const formattedDate = moment(date).fromNow();
			return (
				<View>
					<Card containerStyle={styles.cardContainer}>
						<Tile
							containerStyle={styles.tileContainer}
							imageSrc={{ uri: post.image.src }}
							key={index}
							imageContainerStyle={styles.imageContainer}
							title={(value = post.heading)}
							titleStyle={styles.text}
							onPress={() =>
								navigate('NewsDetail', {
									post: this.props.search.searchResults.list.slice(
										index,
										this.props.search.searchResults.list.length
									)
								})}>
							<Text style={{ flex: 2, textAlign: 'left', fontSize: 15 }}>{formattedDate}</Text>
						</Tile>
					</Card>
				</View>
			);
		};
		// if (this.props.search.isLoading) {
		// 	return <Loading />;
		return (
			<React.Fragment>
				<Animatable.View animation="fadeInRight" duration={200}>
					<Animatable.View
						style={{
							height: 80,
							backgroundColor: '#4682b4',
							justifyContent: 'center',
							paddingHorizontal: 5
						}}>
						<View style={{ height: 50, backgroundColor: 'white', padding: 5, flexDirection: 'row' }}>
							<Icon name="search" type="font-awesome" size={24} />
							<TextInput
								ref={(ref) => (this.textInputRef = ref)}
								placeholder="Search"
								style={{ fontSize: 24, marginLeft: 15, flex: 1 }}
								autoFocus={true}
								onSubmitEditing={Keyboard.dismiss}
								value={this.state.searchTerm}
								clearButtonMode="always"
								onChangeText={(query) => this.onChangeText(query)}
								// onEndEditing={() => this.onSearchTermChange()}
							/>
						</View>
					</Animatable.View>
				</Animatable.View>
				<FlatList
					style={{ flex: 2 }}
					data={this.props.tags.tagNames}
					renderItem={renderTags}
					keyExtractor={(item) => item.id.toString()}
				/>
				<FlatList
					style={{ backgroundColor: 'white' }}
					data={this.props.search.searchResults.list}
					renderItem={renderSearch}
					keyExtractor={(item) => item.id.toString()}
				/>
			</React.Fragment>
		);
	}
}
const styles = StyleSheet.create({
	cardContainer: {
		maxHeight: 100,
		paddingTop: 0,
		paddingHorizontal: 0,
		marginHorizontal: 5,
		marginVertical: 10,
		shadowOffset: { width: 2, height: 2 },
		shadowColor: '#4682b4',
		shadowOpacity: 0.8
	},
	tileContainer: {
		marginHorizontal: 0,
		marginVertical: 0,
		flexDirection: 'row',
		paddingLeft: 0,
		position: 'relative'
	},
	imageContainer: {
		maxHeight: 99,
		maxWidth: 150
	},
	text: {
		maxWidth: 200,
		maxHeight: 50,
		fontSize: 15
	}
});
export default connect(mapStateToProp, mapDispatchToProps)(Search);
