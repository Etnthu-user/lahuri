import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { Card, Button, Icon } from 'react-native-elements';
import { MailComposer } from 'expo';
class Contact extends Component {
	sendMail() {
		MailComposer.composeAsync({
			recipients: [ 'lahurigroup@gmail.com' ],
			subject: 'Enquiry'
		});
	}
	render() {
		return (
			<Card title="Contact Information">
				<View style={{ alignContent: 'center' }}>
					<Text style={styles.text}>8/4 Wyatt Avenue{'\n'}</Text>
					<Text style={styles.text}>Burwood NSW 2134{'\n'}</Text>
					<Text style={styles.text}>Australia{'\n'}</Text>
					<Text style={styles.text}> Tel: 0473 583 929{'\n'}</Text>
					<Button
						title="Send Mail"
						buttonStyle={{ backgroundColor: '#4682b4' }}
						icon={<Icon name="envelope-o" type="font-awesome" color="#ffffff" />}
						onPress={() => this.sendMail()}
					/>
				</View>
			</Card>
		);
	}
}
const styles = StyleSheet.create({
	text: {
		fontSize: 20,
		textAlign: 'center'
	}
});
export default Contact;
