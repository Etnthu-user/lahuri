import React, { Component } from 'react';
import { FlatList, StyleSheet } from 'react-native';
import { Card, Tile } from 'react-native-elements';
import { connect } from 'react-redux';
import Loading from './LoadingComponent';

const mapStateToProps = (state) => {
	return {
		popularPosts: state.popularPosts
	};
};
class PopularPosts extends Component {
	static navigationOptions = {
		title: 'Popular Topics'
	};
	render() {
		const { navigate } = this.props.navigation;
		const renderPopularNewsItem = ({ item, index }) => {
			const popularPosts = { ...item, ...item.postContents[0], ...item.thumbnailImages[0], ...item.isVideo };
			if (popularPosts.isLoading) {
				return <Loading />;
			} else {
				if (popularPosts.isVideo) {
					return (
						<Card containerStyle={styles.cardContainer}>
							<Tile
								containerStyle={styles.tileContainer}
								imageSrc={{ uri: popularPosts.image.src }}
								key={index}
								imageContainerStyle={styles.imageContainer}
								title={(value = popularPosts.heading)}
								titleStyle={styles.title}
								onPress={() =>
									navigate('NewsDetail', {
										post: this.props.popularPosts.popularPosts.slice(
											index,
											this.props.popularPosts.popularPosts.length
										)
									})}
								icon={{ name: 'youtube-play', type: 'font-awesome', size: 50, color: 'white' }}
							/>
						</Card>
					);
				} else {
					return (
						<Card containerStyle={styles.cardContainer}>
							<Tile
								containerStyle={styles.tileContainer}
								imageSrc={{ uri: popularPosts.image.src }}
								key={index}
								imageContainerStyle={styles.imageContainer}
								title={(value = popularPosts.heading)}
								titleStyle={styles.title}
								onPress={() =>
									navigate('NewsDetail', {
										post: this.props.popularPosts.popularPosts.slice(
											index,
											this.props.popularPosts.popularPosts.length
										)
									})}
							/>
						</Card>
					);
				}
			}
		};
		return (
			<FlatList
				data={this.props.popularPosts.popularPosts}
				renderItem={renderPopularNewsItem}
				keyExtractor={(item) => item.id.toString()}
			/>
		);
	}
}
const styles = StyleSheet.create({
	cardContainer: {
		flex: 1,
		maxHeight: 100,
		paddingTop: 0,
		paddingHorizontal: 0,
		marginHorizontal: 0,
		marginVertical: 0,
		marginBottom: 0
	},
	tileContainer: {
		marginHorizontal: 0,
		marginVertical: 0,
		flexDirection: 'row',
		paddingLeft: 0,
		position: 'relative'
	},
	imageContainer: {
		maxHeight: 99,
		maxWidth: 150
	},
	title: {
		maxWidth: 200,
		maxHeight: 50,
		fontSize: 15
	}
});

export default connect(mapStateToProps)(PopularPosts);
