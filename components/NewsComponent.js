import React, { Component } from 'react';
import { FlatList, Text, View } from 'react-native';
import { Tile, Icon } from 'react-native-elements';
import { connect } from 'react-redux';
import Loading from './LoadingComponent';
import { fetchPosts, increaseView } from '../redux/ActionCreator';
import moment from 'moment/min/moment-with-locales';
moment.locale('ne');
const mapDispatchToProps = (dispatch) => ({
	fetchPosts: (StatusId, Page, concat) => dispatch(fetchPosts(StatusId, Page, concat)),
	increaseView: (postId) => dispatch(increaseView(postId))
});
const mapStateToProps = (state) => {
	return {
		posts: state.posts,
		sponsers: state.sponsers
	};
};

class News extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isLoading: false,
			posts: this.props.posts.posts,
			statusId: 2
		};
	}

	componentDidMount() {
		this.props.fetchPosts(this.state.statusId, 1);
	}
	handleRefresh = () => {
		this.setState(
			{
				refreshing: this.props.posts.refreshing
			},
			() => this.props.fetchPosts(this.state.statusId, 1)
		);
	};
	loadMore = () => {
		if (!this.props.posts.isLastPage) {
			this.props.fetchPosts(this.state.statusId, this.props.posts.nextPage, true);
		}
	};
	static navigationOptions = ({ navigation }) => {
		return {
			title: 'Latest Stories'
		};
	};
	render() {
		const { navigate } = this.props.navigation;
		const renderNewsItem = ({ item, index }) => {
			const post = {
				...item,
				...item.postContents[0],
				...item.thumbnailImages[0],
				...item.isVideo,
				...item.postUrl
			};

			const date = post.publishedDate;
			const formattedDate = moment(date).fromNow();
			return post.isVideo ? (
				<React.Fragment>
					<Tile
						title={(value = post.heading)}
						onPress={() =>
							navigate('NewsDetail', {
								postUrl: post.postUrl,
								locale: post.locale.localeName.toLowerCase(),
								newsId: item.id,
								post: this.props.posts.posts.slice(index, this.props.posts.posts.length)
							})}
						imageSrc={(uri = { uri: post.image.src })}
						icon={{ name: 'youtube-play', type: 'font-awesome', size: 100, color: 'white' }}
					/>
					<View
						style={{
							flex: 1,
							flexDirection: 'row',
							justifyContent: 'flex-end'
						}}>
						<Text style={{ flex: 2, textAlign: 'left', fontSize: 15 }}>{formattedDate}</Text>
						<Icon name="eye" type="entypo" size={15} iconStyle={{}} />
						<Text style={{ fontSize: 15 }}> {post.views}</Text>
					</View>
				</React.Fragment>
			) : (
				<React.Fragment>
					<Tile
						title={post.heading}
						onPress={() => {
							this.props.increaseView(item.id);

							navigate('NewsDetail', {
								postUrl: post.postUrl,
								locale: post.locale.localeName.toLowerCase(),
								newsId: item.id,
								post: this.props.posts.posts.slice(index, this.props.posts.posts.length)
							});
						}}
						imageSrc={(uri = { uri: post.image.src })}
						titleStyle={{ textAlign: 'center' }}
					/>
					<View
						style={{
							flex: 1,
							flexDirection: 'row',
							justifyContent: 'flex-end'
						}}>
						<Text style={{ flex: 2, textAlign: 'left', fontSize: 15 }}>{formattedDate}</Text>
						<Icon name="eye" type="entypo" size={15} iconStyle={{}} />
						<Text style={{ fontSize: 15 }}> {post.views}</Text>
					</View>
				</React.Fragment>
			);
		};
		if (this.props.posts && this.props.posts.posts.length) {
			return (
				<React.Fragment>
					<FlatList
						scrollsToTop={true}
						data={this.props.posts.posts}
						renderItem={renderNewsItem}
						keyExtractor={(item) => item.id.toString()}
						onRefresh={this.handleRefresh}
						refreshing={this.props.posts.refreshing}
						onEndReachedThreshold={0.2}
						onEndReached={this.loadMore}
					/>
				</React.Fragment>
			);
		} else if (this.props.posts.isLoading) {
			return <Loading />;
		} else {
			return null;
		}
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(News);
