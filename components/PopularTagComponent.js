import React, { Component } from 'react';
import { Text, FlatList, View, Dimensions, TouchableOpacity, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { fetchTagPosts, fetchSponsers } from '../redux/ActionCreator';
import { Card, Icon } from 'react-native-elements';
import Sponser from './SponserComponent';
import moment from 'moment/min/moment-with-locales';
moment.locale('ne');
const mapStateToProps = (state) => {
	return {
		tagPosts: state.tagPosts,
		sponsers: state.sponsers
	};
};
const mapDispatchToProps = (dispatch) => ({
	fetchTagPosts: (statusId, tagName, Page, append) => dispatch(fetchTagPosts(statusId, tagName, Page, append)),
	fetchSponsers: () => dispatch(fetchSponsers())
});
class PopularTags extends Component {
	constructor(props) {
		super(props);
		this.state = {
			statusId: 2,
			tagName: this.props.navigation.getParam('tagName', '')
		};
	}
	static navigationOptions = ({ navigation }) => ({
		title: navigation.getParam('tagName', '')
	});
	componentDidMount() {
		this.props.fetchTagPosts(this.state.statusId, this.state.tagName, 1);
		this.props.fetchSponsers();
	}
	handleRefresh = () => {
		this.setState(
			{
				refreshing: this.props.tagPosts.refreshing
			},
			() => this.props.fetchTagPosts(this.state.statusId, this.state.tagName)
		);
	};
	loadMore = () => {
		if (!this.props.tagPosts.isLastPage) {
			this.props.fetchTagPosts(this.state.statusId, this.state.tagName, this.props.tagPosts.nextPage, true);
		}
	};
	render() {
		const { navigate } = this.props.navigation;
		const renderTagPosts = ({ item, index }) => {
			const post = { ...item, ...item.thumbnailImages[0], ...item.postContents[0], ...item.isVideo };
			const date = post.publishedDate;
			const formattedDate = moment(date).fromNow();
			return (
				<View style={{ flex: 1, justifyContent: 'flex-start' }}>
					<TouchableOpacity
						onPress={() => {
							navigate('TagPostDetail', {
								tagPostId: item.id,
								post: this.props.tagPosts.tagPosts.slice(index, this.props.tagPosts.tagPosts.length)
							});
						}}>
						<Card title={post.heading} image={{ uri: post.image.src }} containerStyle={styles.container}>
							<View
								style={{
									flex: 1,
									flexDirection: 'row',
									justifyContent: 'flex-end'
								}}>
								<Text style={{ flex: 2, textAlign: 'left', fontSize: 15 }}>{formattedDate}</Text>
								<Icon name="eye" type="entypo" size={15} iconStyle={{}} />
								<Text style={{ fontSize: 15 }}> {post.views}</Text>
							</View>
						</Card>
					</TouchableOpacity>
				</View>
			);
		};

		return (
			<React.Fragment>
				<FlatList
					data={this.props.tagPosts.tagPosts}
					renderItem={renderTagPosts}
					keyExtractor={(item) => item.id.toString()}
					refreshing={this.props.tagPosts.refreshing}
					onRefresh={this.handleRefresh}
					onEndReached={this.loadMore}
					onEndReachedThreshold={0.1}
					numColumns={1}
				/>
				<Sponser />
			</React.Fragment>
		);
	}
}
const styles = StyleSheet.create({
	container: {
		// maxWidth: Dimensions.get('window').width,
		flexGrow: 1,
		marginHorizontal: 5,
		marginVertical: 10,
		shadowOffset: { width: 5, height: 5 },
		shadowColor: '#4682b4',
		shadowOpacity: 0.5
	}
});
export default connect(mapStateToProps, mapDispatchToProps)(PopularTags);
