import React, { Component } from 'react';
import { View, Image } from 'react-native';

class FullScreenImage extends Component {
	static navigationOptions = () => {
		return {
			title: 'Image'
		};
	};
	render() {
		const image = this.props.navigation.getParam('image', null);
		return (
			<View style={{ flex: 1, backgroundColor: 'black' }}>
				<Image
					style={{
						flex: 1,
						height: null,
						width: null,
						justifyContent: 'center',
						alignSelf: 'stretch',
						alignItems: 'center',
						resizeMode: 'contain'
					}}
					source={{ uri: image }}
				/>
			</View>
		);
	}
}

export default FullScreenImage;
