import React, { Component } from 'react';
import {
	Text,
	ScrollView,
	StyleSheet,
	WebView,
	Share,
	View,
	FlatList,
	Dimensions,
	Image,
	TouchableOpacity
} from 'react-native';
import { Card, Icon, Button } from 'react-native-elements';
import { connect } from 'react-redux';
import Sponser from './SponserComponent';
import HTML from 'react-native-render-html';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;
const mapStateToProps = (state) => {
	return {
		posts: state.posts,
		tagPosts: state.tagPosts
	};
};
class NewsDetail extends Component {
	static navigationOptions = ({ navigation }) => {
		return {
			title: '',
			headerRight: (
				<Icon
					name="share-square-o"
					type="font-awesome"
					color="white"
					size={30}
					iconStyle={{ paddingRight: 20 }}
					onPress={navigation.getParam('shareNews')}
				/>
			)
		};
	};

	componentDidMount() {
		this.props.navigation.setParams({ shareNews: this.shareNews });
	}
	shareNews = () => {
		Share.share({
			title: 'Lahuri News',
			url: `http://lahuritv.net/ne-np/post/${this.props.navigation.getParam('newsId', '')}`
		});
	};
	usePostUrl = (postUrl) => {};
	render() {
		const { navigate } = this.props.navigation;
		const post = this.props.navigation.getParam('post', '');
		renderPosts = ({ item, index }) => {
			const postWithContent = {
				...item,
				...item.postContents[0],
				...item.thumbnailImages[0],
				...item.isVideo
			};

			if (postWithContent.isVideo) {
				return (
					<ScrollView>
						<View style={{ flex: 1, height: SCREEN_HEIGHT, width: SCREEN_WIDTH }}>
							<ScrollView>
								<Card title={postWithContent.heading}>
									<WebView
										source={{ uri: postWithContent.videoEmbedUrl }}
										style={{ aspectRatio: 1.7 }}
									/>
									<View style={styles.container}>
										{postWithContent.postTags.map((tag) => (
											<Button
												buttonStyle={styles.button}
												style={{ paddingHorizontal: 3 }}
												iconContainerStyle={{ marginRight: 0.1 }}
												icon={<Icon name="tag" type="font-awesome" size={20} color="#4682b4" />}
												key={tag.id}
												title={tag.tagName}
												titleStyle={{ color: '#4682b4' }}
												onPress={() => navigate('PopularTags', { tagName: tag.tagName })}
											/>
										))}
									</View>
									<HTML
										html={postWithContent.content}
										tagsStyles={{
											p: { fontSize: 17 },
											img: { maxWidth: Dimensions.get('window').width - 30 }
										}}
									/>
									<Text style={{ textAlign: 'right', fontSize: 15, fontStyle: 'italic' }}>
										Published Date:{new Intl.DateTimeFormat('en-US', {
											year: 'numeric',
											month: 'short',
											day: '2-digit'
										}).format(new Date(Date.parse(postWithContent.publishedDate)))}
									</Text>
								</Card>
							</ScrollView>
						</View>
					</ScrollView>
				);
			}
			return (
				<View>
					<View style={{ flex: 1, height: SCREEN_HEIGHT, width: SCREEN_WIDTH }}>
						<ScrollView>
							<Card containerStyle={{ marginHorizontal: 0, marginVertical: 0 }}>
								<TouchableOpacity
									onPress={() => navigate('FullScreenImage', { image: postWithContent.image.src })}>
									<Image source={{ uri: postWithContent.image.src }} style={{ aspectRatio: 1.2 }} />
								</TouchableOpacity>
								<Text style={styles.header}>{postWithContent.heading}</Text>
								<View style={styles.container}>
									{postWithContent.postTags.map((tag) => (
										<Button
											buttonStyle={styles.button}
											style={{ paddingHorizontal: 3 }}
											iconContainerStyle={{ marginRight: 0.1 }}
											icon={<Icon name="tag" type="font-awesome" size={20} color="#4682b4" />}
											key={tag.id}
											title={tag.tagName}
											titleStyle={{ color: '#4682b4' }}
											onPress={() => navigate('PopularTags', { tagName: tag.tagName })}
										/>
									))}
								</View>
								<HTML
									html={postWithContent.content}
									tagsStyles={{
										p: { fontSize: 17 },
										img: { maxWidth: Dimensions.get('window').width - 30 }
									}}
								/>
								<Text style={{ textAlign: 'right', fontSize: 15, fontStyle: 'italic' }}>
									Published Date:{new Intl.DateTimeFormat('en-US', {
										year: 'numeric',
										month: 'short',
										day: '2-digit'
									}).format(new Date(Date.parse(postWithContent.publishedDate)))}
								</Text>
							</Card>
						</ScrollView>
					</View>
				</View>
			);
		};
		return (
			<React.Fragment>
				<FlatList
					data={post}
					renderItem={renderPosts}
					keyExtractor={(post) => post.id.toString()}
					pagingEnabled={true}
					horizontal={true}
				/>
				<Sponser />
			</React.Fragment>
		);
	}
}
const styles = StyleSheet.create({
	header: {
		fontSize: 25,
		textAlign: 'center'
	},
	container: {
		flexDirection: 'row',
		alignContent: 'flex-start',
		justifyContent: 'center'
	},
	button: {
		backgroundColor: '#e9f2e8',
		borderRadius: 20
	},
	NoFoundText: {
		fontSize: 29,
		marginTop: 400,
		textAlign: 'center'
	}
});
export default connect(mapStateToProps)(NewsDetail);
