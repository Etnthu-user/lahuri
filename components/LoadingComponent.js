import React from 'react';
import { StyleSheet, ActivityIndicator, View, Text } from 'react-native';
const Loading = () => {
	return (
		<View style={styles.container}>
			<ActivityIndicator size="large" color="#4682b4" />
			<Text style={{ fontSize: 20, fontWeight: 'bold', textAlign: 'center' }} color="#4682b4">
				Loading . . .{' '}
			</Text>
		</View>
	);
};
const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center'
	}
});
export default Loading;
