import React, { Component } from 'react';
import {
	createStackNavigator,
	createDrawerNavigator,
	DrawerItems,
	SafeAreaView,
	NavigationActions
} from 'react-navigation';
import { Icon } from 'react-native-elements';
import { connect } from 'react-redux';
import { fetchPopularPosts, fetchSponsers } from '../redux/ActionCreator';
import News from './NewsComponent';
import NewsDetail from './NewsDetailComponent';
import { View, Platform, ScrollView, StyleSheet, Image } from 'react-native';
import PopularPosts from './PopularPostComponent';
import PopularNewsDetail from './PopularPostDetailComponent';
import Videos from './VideosComponent';
import VideosDetail from './VideosDetailComponent';
import RenderVideo from './RenderEpisodeComponent';
import PopularTags from './PopularTagComponent';
import TagPostDetail from './TagPostsDetailComponent';
import Contact from './ContactComponent';
import Search from './SearchComponent';
import FullScreenImage from './FullScreenImage';

const mapStateToProps = (state) => {
	return {
		// posts: state.posts
	};
};

const mapDispatchToProps = (dispatch) => ({
	fetchPopularPosts: () => dispatch(fetchPopularPosts()),
	fetchSponsers: () => dispatch(fetchSponsers())
});

const NewsNavigator = createStackNavigator(
	{
		News: {
			screen: News,
			navigationOptions: ({ navigation }) => ({
				headerRight: (
					<Icon name="search" size={40} color="white" onPress={() => navigation.navigate('Search')} />
				),
				headerLeft: <Icon name="menu" size={50} color="white" onPress={() => navigation.toggleDrawer()} />
			})
		},

		NewsDetail: { screen: NewsDetail },
		PopularTags: {
			screen: PopularTags
		},
		FullScreenImage: {
			screen: FullScreenImage
		}
	},
	{
		initialRouteName: 'News',
		navigationOptions: {
			headerStyle: { backgroundColor: '#4682b4' },
			headerTintColor: '#fff',
			headerTitleStyle: {
				color: '#fff',
				fontSize: 20
			}
		}
	}
);
const SearchNavigator = createStackNavigator({
	Search: {
		screen: Search,
		navigationOptions: ({ navigation }) => ({
			headerStyle: { backgroundColor: '#4682b4' },
			headerTintColor: '#fff',
			headerTitleStyle: {
				color: '#fff',
				fontSize: 20
			},
			headerLeft: (
				<Icon
					name="cross"
					type="entypo"
					size={50}
					color="white"
					onPress={() => navigation.navigate(NavigationActions.navigate({ routeName: 'News' }))}
				/>
			)
		})
	}
});
const PopularNewsNavigator = createStackNavigator(
	{
		PopularPosts: {
			screen: PopularPosts,
			navigationOptions: ({ navigation }) => ({
				headerLeft: <Icon name="menu" size={50} color="white" onPress={() => navigation.toggleDrawer()} />
			})
		},
		PopularNewsDetail: {
			screen: PopularNewsDetail
		},
		TagPostDetail: {
			screen: TagPostDetail
		}
	},
	{
		initialRouteName: 'PopularPosts',
		navigationOptions: {
			headerStyle: { backgroundColor: '#4682b4' },
			headerTintColor: '#ffff',
			headerTitleStyle: {
				color: '#fff',
				fontSize: 20
			}
		}
	}
);
const VideosNavigator = createStackNavigator(
	{
		Videos: {
			screen: Videos,
			navigationOptions: ({ navigation }) => ({
				headerLeft: <Icon name="menu" size={40} color="white" onPress={() => navigation.toggleDrawer()} />
			})
		},
		VideosDetail: {
			screen: VideosDetail
		},
		RenderVideo: {
			screen: RenderVideo
		}
	},
	{
		initialRouteName: 'Videos',
		navigationOptions: {
			headerStyle: { backgroundColor: '#4682b4' },
			headerTintColor: '#ffffff',
			headerBackTitleStyle: {
				color: '#ffffff',
				fontSize: 20
			}
		}
	}
);
const ContactNavigator = createStackNavigator(
	{
		Contact: {
			screen: Contact,
			navigationOptions: ({ navigation }) => ({
				headerLeft: <Icon name="menu" size={40} color="white" onPress={() => navigation.toggleDrawer()} />
			})
		}
	},
	{
		initialRouteName: 'Contact',
		navigationOptions: {
			headerStyle: { backgroundColor: '#4682b4' },
			headerTintColor: '#ffffff',
			headerBackTitleStyle: {
				color: '#ffffff',
				fontSize: 20
			}
		}
	}
);
const CustomDrawerContentComponent = (props) => (
	<ScrollView>
		<SafeAreaView style={styles.container} forceInset={{ top: 'always', horizontal: 'never' }}>
			<View style={styles.drawerHeader}>
				<View style={{ flex: 1 }}>
					<Image source={require('../shared/ltv-logo.png')} style={styles.drawerImage} />
				</View>
			</View>
			<DrawerItems {...props} />
		</SafeAreaView>
	</ScrollView>
);
const MainNavigator = createDrawerNavigator(
	{
		News: {
			screen: NewsNavigator,
			navigationOptions: {
				title: 'News',
				drawLabel: 'News',
				drawerIcon: ({ tintColor }) => <Icon name="news" type="entypo" size={24} color={tintColor} />
			}
		},
		PopularPosts: {
			screen: PopularNewsNavigator,
			navigationOptions: {
				title: 'Popular Posts',
				drawLabel: 'Popular Posts',
				drawerIcon: ({ tintColor }) => <Icon name="thumbs-up" type="entypo" size={24} color={tintColor} />
			}
		},
		Videos: {
			screen: VideosNavigator,
			navigationOptions: {
				title: 'TV Shows',
				drawLabel: 'Videos',
				drawerIcon: ({ tintColor }) => <Icon name="video" type="entypo" size={24} color={tintColor} />
			}
		},
		Contact: {
			screen: ContactNavigator,
			navigationOptions: {
				title: 'Contact',
				drawLabel: 'Contact',
				drawerIcon: ({ tintColor }) => (
					<Icon name="address-card" type="font-awesome" size={24} color={tintColor} />
				)
			}
		},
		Search: {
			screen: SearchNavigator,
			navigationOptions: {
				title: 'Search',
				drawLabel: 'Search',
				drawerIcon: ({ tintColor }) => <Icon name="search" type="font-awesome" size={24} color={tintColor} />
			}
		}
	},
	{
		initialRouteName: 'News',
		drawerBakgroundColor: '#4682b4',
		contentComponent: CustomDrawerContentComponent
	}
);
class Main extends Component {
	componentDidMount() {
		this.props.fetchPopularPosts();
		this.props.fetchSponsers();
	}
	render() {
		return (
			<View style={{ flex: 1, paddingTop: Platform.OS === 'ios' ? 0 : Expo.Constants.statusBarHeight }}>
				<MainNavigator />
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	drawerHeader: {
		backgroundColor: '#ffffff',
		marginLeft: 50,
		height: 100,
		alignItems: 'center',
		justifyContent: 'center',
		flex: 1,
		flexDirection: 'row'
	},
	drawerHeaderText: {
		color: 'white',
		fontSize: 24,
		fontWeight: 'bold'
	},
	drawerImage: {
		// backgroundColor: '#ffffff',

		width: 300 / 2,
		height: 137 / 2

		// resizeMode: 'contain'
	}
});

export default connect(mapStateToProps, mapDispatchToProps)(Main);
