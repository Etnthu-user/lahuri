import React from 'react';
import { WebView, StyleSheet } from 'react-native';

function RenderVideo(props) {
	const video = props.navigation.getParam('video', null);
	return video && <WebView source={{ uri: video.embedUrl }} contentInset={styles.content} />;
}
RenderVideo.navigationOptions = {
	title: 'Australia React'
};
const styles = StyleSheet.create({
	content: {
		top: 10,
		bottom: 10,
		left: 10,
		right: 10
	}
});
export default RenderVideo;
