import React, { Component } from 'react';
import { Text, FlatList } from 'react-native';
import { connect } from 'react-redux';
import Loading from './LoadingComponent';
import { fetchShows } from '../redux/ActionCreator';
import { Tile } from 'react-native-elements';

const mapDispatchToProps = (dispatch) => ({
	fetchShows: () => dispatch(fetchShows())
});

const mapStateToProps = (state) => {
	return {
		shows: state.shows
	};
};

class Videos extends Component {
	static navigationOptions = {
		title: 'Shows'
	};

	componentDidMount() {
		this.props.fetchShows();
	}
	render() {
		const { navigate } = this.props.navigation;
		const renderVideos = ({ item }) => {
			const show = { ...item, ...item.thumbnailImages[0] };
			if (this.props.shows.isLoading) {
				return <Loading />;
			} else if (this.props.shows.shows.length) {
				return (
					<Tile
						titleStyle={{ textAlign: 'center' }}
						title={(value = show.name)}
						onPress={() => navigate('VideosDetail', { showId: item.id })}
						imageSrc={{ uri: show.image.src }}
					/>
				);
			} else {
				return (
					<Text style={{ fontSize: 30, textAlign: 'center', paddingTop: 300 }}>
						OOps!!! Something Wrong. That means it's being fixed.Sorry for the Inconvenience.
					</Text>
				);
			}
		};
		return (
			<FlatList
				data={this.props.shows.shows}
				renderItem={renderVideos}
				keyExtractor={(item) => item.id.toString()}
			/>
		);
	}
}
export default connect(mapStateToProps, mapDispatchToProps)(Videos);
