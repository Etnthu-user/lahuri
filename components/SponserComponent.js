import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Modal, TouchableOpacity, StyleSheet, Dimensions, Text, Image, Linking, Alert, View } from 'react-native';
import { Card, Button, Icon } from 'react-native-elements';
import * as Animatable from 'react-native-animatable';
const mapStateToProps = (state) => {
	return {
		sponsers: state.sponsers
	};
};
class Sponser extends Component {
	constructor(props) {
		super(props);
		this.state = {
			showModal: true
		};
	}
	static navigationOptions = ({ navigation }) => {
		return {
			title: 'Advertisement'
		};
	};
	toggleModal = () => {
		this.setState({ showModal: !this.state.showModal });
	};
	goToWeb = (url) => {
		return <SponserSite url={url} />;
	};
	render() {
		const newList = new Array();
		const sponserLength = this.props.sponsers.sponsers.list.length;
		for (var i = 0; i < sponserLength; i++) {
			if (Math.random() > 0.5) {
				let newIndex = Math.ceil(Math.random() * sponserLength);
				while (newList.length < 1) {
					newList.push(this.props.sponsers.sponsers.list[newIndex]);
					break;
				}
			}
		}
		const sponser = { ...newList[0] };
		if (sponser.hasOwnProperty('title')) {
			return (
				<Modal
					onRequestClose={this.toggleModal}
					animationType={'fade'}
					transparent={false}
					visible={this.state.showModal}>
					<Animatable.View animation="fadeInLeft" duration={500}>
						<TouchableOpacity
							onPress={() =>
								Alert.alert(
									'Visit a Website',
									"You will be redirected to the advertiser's website ",
									[
										{
											text: 'Cancel',
											onPress: () => this.toggleModal(),
											style: 'cancel'
										},
										{
											text: 'Ok',
											onPress: () => {
												Linking.openURL(sponser.adUrl), this.toggleModal();
											}
										}
									],
									{ cancelable: false }
								)}>
							<Card containerStyle={styles.modalCard} title={sponser.title}>
								<Image
									style={{
										width: Dimensions.get('window').width - 30,
										minHeight: Dimensions.get('window').height / 3,
										resizeMode: 'contain'
									}}
									source={{ uri: sponser.thumbnailImageUrl }}
								/>
								<Card>
									<Text style={styles.modalText}>{sponser.tagLine}</Text>
								</Card>
							</Card>
						</TouchableOpacity>
						{/* <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}> */}
						<Button
							buttonStyle={{
								backgroundColor: '#4682b4',
								borderColor: 'red',
								width: Dimensions.get('window').width,
								height: 50
							}}
							onPress={() => this.toggleModal()}
							color="#c0c0c0"
							title="Continue reading"
							iconRight={true}
							icon={<Icon name="angle-double-right" type="font-awesome" size={22} iconStyle={{}} />}
						/>
						{/* </View> */}
					</Animatable.View>
				</Modal>
			);
		} else {
			return null;
		}
	}
}

const styles = StyleSheet.create({
	modalCard: {
		borderTopColor: 'white',
		marginTop: 100,
		// paddingTop: 10,
		minWidth: Dimensions.get('screen').width,
		// minHeight: Dimensions.get('screen').height / 2,
		marginLeft: 0
	},
	modalText: {
		textAlign: 'center',
		fontSize: 20
	}
});

export default connect(mapStateToProps)(Sponser);
