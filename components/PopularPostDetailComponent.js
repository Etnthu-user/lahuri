import React, { Component } from 'react';
import { WebView, Text, ScrollView, Dimensions, Share, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { Card, Icon } from 'react-native-elements';
import HTML from 'react-native-render-html';

const mapStateToProps = (state) => {
	return {
		popularPosts: state.popularPosts
	};
};
class PopularNewsDetail extends Component {
	constructor(props) {
		super(props);
	}
	static navigationOptions = ({ navigation }) => {
		return {
			title: '',
			headerRight: (
				<Icon
					name="share-square-o"
					type="font-awesome"
					color="white"
					size={30}
					iconStyle={{ paddingRight: 20 }}
					onPress={navigation.getParam('shareNews')}
				/>
			)
		};
	};

	componentDidMount() {
		this.props.navigation.setParams({ shareNews: this.shareNews });
	}
	shareNews = () => {
		Share.share({
			title: 'Lahuri News',
			url: `http://lahuritv.net/ne-np/post/${this.props.navigation.getParam('newsId', '')}`
		});
	};

	render() {
		const popularNewsId = this.props.navigation.getParam('popularNewsId', '');
		return (
			<ScrollView>
				<RenderPopularNews
					popularPost={this.props.popularPosts.popularPosts.find(
						(p) => p.id.toString() === popularNewsId.toString()
					)}
				/>
			</ScrollView>
		);
	}
}

function RenderPopularNews(props) {
	const popularPost = props.popularPost;
	if (popularPost) {
		const postWithContent = {
			...popularPost,
			...popularPost.postContents[0],
			...popularPost.thumbnailImages[0],
			...popularPost.isVideo
		};
		if (popularPost.isVideo) {
			return (
				<Card title={postWithContent.heading}>
					<WebView source={{ uri: popularPost.videoEmbedUrl }} style={styles.web} />
					<Text style={styles.text}>
						Published Date:{new Intl.DateTimeFormat('en-US', {
							year: 'numeric',
							month: 'short',
							day: '2-digit'
						}).format(new Date(Date.parse(popularPost.publishedDate)))}
					</Text>
					<HTML
						html={postWithContent.content}
						tagsStyles={{
							p: { fontSize: 17 },
							img: { maxWidth: Dimensions.get('window').width - 30 }
						}}
					/>
				</Card>
			);
		} else {
			return (
				<Card
					containerStyle={styles.container}
					imageStyle={styles.imageContainer}
					image={{ uri: postWithContent.image.src }}>
					<Text style={styles.heading}>{postWithContent.heading}</Text>

					<Text style={styles.text}>
						Published Date:{new Intl.DateTimeFormat('en-US', {
							year: 'numeric',
							month: 'short',
							day: '2-digit'
						}).format(new Date(Date.parse(popularPost.publishedDate)))}
					</Text>
					<HTML
						html={postWithContent.content}
						tagsStyles={{
							p: { fontSize: 17 },
							img: { maxWidth: Dimensions.get('window').width - 30 }
						}}
					/>
				</Card>
			);
		}
	}
}
const styles = StyleSheet.create({
	text: {
		textAlign: 'right',
		fontSize: 15,
		fontStyle: 'italic'
	},
	container: {
		flex: 1,
		marginHorizontal: 0,
		marginVertical: 0
	},
	imageContainer: {
		justifyContent: 'center',
		height: 400
	},
	heading: {
		fontSize: 25,
		textAlign: 'center',
		fontWeight: '900'
	},
	web: {
		flex: 1,
		aspectRatio: 1.7
	}
});
export default connect(mapStateToProps)(PopularNewsDetail);
