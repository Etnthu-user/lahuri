import * as ActionTyes from './ActionTypes';

export const popularPosts = (
	state = {
		isLoading: true,
		error: null,
		popularPosts: []
	},
	action
) => {
	switch (action.type) {
		case ActionTyes.GET_POPULAR_POST:
			return { ...state, isLoading: false, error: null, popularPosts: action.payload };
		case ActionTyes.POPULAR_POST_LOADING:
			return { ...state, isLoading: true, error: null, popularPosts: [] };
		case ActionTyes.POPULAR_POST_FAILED:
			return { ...state, isLoading: false, error: action.payload };
		default:
			return state;
	}
};
