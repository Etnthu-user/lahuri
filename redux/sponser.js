import * as ActionTypes from './ActionTypes';

export const sponsers = (
	state = {
		isLoading: true,
		error: null,
		sponsers: []
	},
	action
) => {
	switch (action.type) {
		case ActionTypes.GET_SPONSERS:
			return { ...state, isLoading: false, error: null, sponsers: action.payload };
		case ActionTypes.SPONSERS_LOADING:
			return { ...state, isLoading: true, error: null };
		case ActionTypes.SPONSERS_FALIED:
			return { ...state, isLoading: false, error: action.payload };
		default:
			return state;
	}
};
