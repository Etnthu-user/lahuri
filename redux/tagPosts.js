import * as ActionTypes from './ActionTypes';

export const tagPosts = (
	state = {
		nextPage: 2,
		isLastPage: false,
		refreshing: true,
		isLoading: true,
		error: null,
		tagPosts: []
	},
	action
) => {
	switch (action.type) {
		case ActionTypes.GET_TAGSPOST:
			return {
				...state,
				isLoading: false,
				refreshing: false,
				error: null,
				isLastPage: action.payload.tagPosts.isLastPage,
				nextPage: action.payload.tagPosts.nextPage,

				tagPosts: action.payload.append
					? state.tagPosts.concat(action.payload.tagPosts.list)
					: action.payload.tagPosts.list
			};
		case ActionTypes.TAGPOSTS_LOADING:
			return { ...state, isLoading: true, refreshing: false, error: null };
		case ActionTypes.TAGPOSTS_FAILED:
			return { ...state, isLoading: false, error: action.payload };
		default:
			return state;
	}
};
