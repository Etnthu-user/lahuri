import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { posts } from './post';
import { popularPosts } from './popularPost';
import { sponsers } from './sponser';
import { shows } from './shows';
import { seasons } from './seasons';
import { episodes } from './episodes';
import { tagPosts } from './tagPosts';
import { search } from './search';
import { tags } from './tags';

export const ConfigureStore = () => {
	const store = createStore(
		combineReducers({
			posts,
			popularPosts,
			sponsers,
			shows,
			seasons,
			episodes,
			tagPosts,
			search,
			tags
		}),
		applyMiddleware(thunk)
	);
	return store;
};
