import * as ActionTypes from './ActionTypes';

export const posts = (
	state = {
		nextPage: 2,
		isLastPage: false,
		isLoading: true,
		refreshing: true,
		error: null,
		posts: []
	},
	action
) => {
	switch (action.type) {
		case ActionTypes.ADD_POST:
			return {
				...state,
				isLoading: false,
				refreshing: false,
				error: null,
				isLastPage: action.payload.pagedPostList.isLastPage,
				nextPage: action.payload.pagedPostList.nextPage,
				posts: action.payload.append
					? state.posts.concat(action.payload.pagedPostList.list)
					: action.payload.pagedPostList.list
			};
		case ActionTypes.POST_LOADING:
			return { ...state, isLoading: true, refreshing: false, error: null };
		case ActionTypes.POST_FAILED:
			return { ...state, isLoading: false, error: action.payload };
		case ActionTypes.INCREASE_VIEW:
			return { ...state };
		default:
			return state;
	}
};
