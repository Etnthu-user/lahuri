import * as ActionTypes from './ActionTypes';

export const episodes = (
	state = {
		nextPage: 'CBQQAA',
		errMess: null,
		episodes: [],
		isLastPage: false,
		isLoading: true,
		refreshing: true
	},
	action
) => {
	switch (action.type) {
		case ActionTypes.GET_EPISODES:
			return {
				...state,
				isLoading: false,
				refreshing: false,
				errMess: null,
				isLastPage: action.payload.episodes.isLastPage,
				nextPage: action.payload.episodes.nextPage,
				episodes: action.payload.append
					? state.episodes.concat(action.payload.episodes.list)
					: action.payload.episodes.list
			};

		case ActionTypes.EPISODES_LOADING:
			return { ...state, isLoading: true };
		case ActionTypes.EPISODES_FAILED:
			return { ...state, isLoading: false, errMess: action.payload };
		default:
			return state;
	}
};
