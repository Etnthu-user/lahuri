import * as ActionTypes from './ActionTypes';

export const tags = (
	state = {
		isLoading: true,
		error: null,
		tagNames: []
	},
	action
) => {
	switch (action.type) {
		case ActionTypes.GET_TAGS:
			return { ...state, isLoading: false, tagNames: action.payload };
		case ActionTypes.TAGS_LOADING:
			return { ...state, isLoading: true };
		case ActionTypes.TAGS_FAILED:
			return { ...state, isLoading: false, error: action.payload };
		default:
			return state;
	}
};
