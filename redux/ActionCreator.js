import * as ActionTypes from './ActionTypes';
import { baseUrl } from '../shared/baseUrl';

//Get posts
export const fetchPosts = (StatusId, Page, append) => (dispatch) => {
	dispatch(postsLoading());
	return fetch(baseUrl + `post?StatusId=${StatusId}&Page=${Page}`)
		.then(
			(response) => {
				if (response.ok) {
					return response;
				} else {
					var err = new Error('Error ' + response.status + ' : ' + response.statusText);
					err.message = response;
					throw err;
				}
			},
			(error) => {
				var errrMessage = new Error(error.message);
				throw errrMessage;
			}
		)
		.then((response) => response.json())
		.then((pagedPostList) => dispatch(addPosts(pagedPostList, append)))
		.catch((error) => dispatch(postsFailed(error.message)));
};
export const postsLoading = () => ({
	type: ActionTypes.POST_LOADING
});
export const addPosts = (pagedPostList, append) => ({
	type: ActionTypes.ADD_POST,
	payload: { pagedPostList, append }
});
export const postsFailed = (err) => ({
	type: ActionTypes.POST_FAILED,
	payload: err
});
//Get Posts Corresponding to the tagname
export const fetchTagPosts = (statusId, tagName, Page, append) => (dispatch) => {
	dispatch(tagPostsLoading());
	return fetch(baseUrl + `post?StatusId=${statusId}&Tags=${tagName}&Page=${Page}`)
		.then(
			(response) => {
				if (response.ok) {
					return response;
				} else {
					const error = new Error('Error + ' + response.status + ' : ' + response.statusText);
					error.message = response;
					throw error;
				}
			},
			(error) => {
				var errMessage = new Error(error.message);
				throw errMessage;
			}
		)
		.then((response) => response.json())
		.then((tagPosts) => dispatch(getTagPosts(tagPosts, append)))
		.catch((error) => dispatch(tagPostsFailed(error.message)));
};
export const tagPostsLoading = () => ({
	type: ActionTypes.TAGPOSTS_LOADING
});
export const getTagPosts = (tagPosts, append) => ({
	type: ActionTypes.GET_TAGSPOST,
	payload: { tagPosts, append }
});
export const tagPostsFailed = (error) => ({
	type: ActionTypes.TAGPOSTS_FAILED,
	paylaod: error
});

//Get popular posts

export const fetchPopularPosts = () => (dispatch) => {
	dispatch(popularPostsLoading());
	return fetch(baseUrl + 'post/popular/20')
		.then(
			(response) => {
				if (response.ok) {
					return response;
				} else {
					var err = new Error('Error ' + response.status + ' : ' + response.statusText);
					err.message = response;
					throw err;
				}
			},
			(error) => {
				var errrMessage = new Error(error.message);
				throw errrMessage;
			}
		)
		.then((response) => response.json())
		.then((popularPosts) => dispatch(getPopularPosts(popularPosts)))
		.catch((error) => dispatch(popularPostsfailed(error.message)));
};
export const popularPostsLoading = () => ({
	type: ActionTypes.POPULAR_POST_LOADING
});
export const getPopularPosts = (popularPosts) => ({
	type: ActionTypes.GET_POPULAR_POST,
	payload: popularPosts
});
export const popularPostsfailed = (error) => ({
	type: ActionTypes.POPULAR_POST_FAILED,
	payload: error
});

//Get sponsers
export const fetchSponsers = () => (dispatch) => {
	dispatch(sponsersLoading());
	return fetch(baseUrl + 'sponsers')
		.then(
			(response) => {
				if (response.ok) {
					return response;
				} else {
					var error = new Error('Error ' + response.status + ' : ' + response.statusText);
					error.message = response;
					throw error;
				}
			},
			(error) => {
				var errrMessage = new Error(error.message);
				throw errrMessage;
			}
		)
		.then((response) => response.json())
		.then((sponsers) => dispatch(getSponsers(sponsers)))
		.catch((error) => dispatch(sponsersFailed(error.message)));
};

export const sponsersLoading = () => ({
	type: ActionTypes.SPONSERS_LOADING
});
export const getSponsers = (sponsers) => ({
	type: ActionTypes.GET_SPONSERS,
	payload: sponsers
});
export const sponsersFailed = (error) => ({
	type: ActionTypes.SPONSERS_FALIED,
	payload: error
});

//Get videos
export const fetchShows = () => (dispatch) => {
	dispatch(showsLoading());
	// const API_KEY = 'AIzaSyDZRtUCtBwBUTeiMYg2YbyWego743N1_Ik';
	// const channelId = 'UCjKONLm4hUp8iA8WJTSmKjg';
	return fetch(baseUrl + 'shows')
		.then(
			(response) => {
				if (response.ok) {
					return response;
				} else {
					var error = new Error('Error ' + response.status + ' : ' + response.statusText);
					error.message = response;
					throw error;
				}
			},
			(error) => {
				var errMess = new Error(error.message);
				throw errMess;
			}
		)
		.then((response) => response.json())
		.then((videos) => dispatch(getShows(videos)))
		.catch((error) => dispatch(showsFailed(error.message)));
};
export const showsLoading = () => ({
	type: ActionTypes.SHOWS_LOADING
});
export const getShows = (shows) => ({
	type: ActionTypes.GET_SHOWS,
	payload: shows
});
export const showsFailed = (error) => ({
	type: ActionTypes.SHOWS_FAILED,
	payload: error
});

//Get seasons
export const fetchSeasons = (showId) => (dispatch) => {
	dispatch(seasonsLoading());
	return fetch(baseUrl + `shows/${showId}/seasons`)
		.then(
			(response) => {
				if (response.ok) {
					return response;
				} else {
					var error = new Error('Error ' + response.status + ' : ' + response.statusText);
					error.message = response;
					throw error;
				}
			},
			(error) => {
				var errMess = new Error(error.message);
				throw errMess;
			}
		)
		.then((response) => response.json())
		.then((seasons) => dispatch(getSeasons(seasons)))
		.catch((error) => dispatch(seasonsFailed(error.message)));
};
export const getSeasons = (seasons) => ({
	type: ActionTypes.GET_SEASONS,
	payload: seasons
});

export const seasonsLoading = () => ({
	type: ActionTypes.SEASONS_LOADING
});
export const seasonsFailed = (error) => ({
	type: ActionTypes.SEASONS_FAILED,
	payload: error
});

//Get Episodes
export const fetchEpisodes = (seasonId, Page, append) => (dispatch) => {
	dispatch(episodesLoading());
	return fetch(baseUrl + `seasons/${seasonId}/episodes?Page=${Page}`)
		.then(
			(response) => {
				if (response.ok) {
					return response;
				} else {
					var error = new Error('Error ' + response.status + ' : ' + response.statusText);
					error.message = response;
					throw error;
				}
			},
			(error) => {
				var errMess = new Error(error.messsage);

				throw errMess;
			}
		)
		.then((response) => response.json())
		.then((episodes) => dispatch(getEpisodes(episodes, append)))
		.catch((error) => dispatch(episodesFailed(error.message)));
};

export const episodesLoading = () => ({
	type: ActionTypes.EPISODES_LOADING
});
export const getEpisodes = (episodes, append) => ({
	type: ActionTypes.GET_EPISODES,
	payload: { episodes, append }
});
export const episodesFailed = (error) => ({
	type: ActionTypes.EPISODES_FAILED,
	payload: error
});

//Search Query Action
export const fetchSearchResults = (statusId, search) => (dispatch) => {
	dispatch(searchLoading());
	return fetch(baseUrl + `post?StatusId=${statusId}&Search=${search}`)
		.then(
			(response) => {
				if (response.ok) {
					return response;
				} else {
					var error = new Error('Error ' + response.status + ' : ' + response.statusText);
					error.message = response;
					throw error;
				}
			},
			(err) => {
				var err = new Error(err.message);
				throw err;
			}
		)
		.then((response) => response.json())
		.then((results) => dispatch(getSearchResults(results)))
		.catch((err) => dispatch(searchFailed(err.message)));
};

export const searchLoading = () => ({
	type: ActionTypes.SEARCH_LOADING
});
export const getSearchResults = (results) => ({
	type: ActionTypes.GET_SEARCHRESULTS,
	payload: results
});
export const searchFailed = (err) => ({
	type: ActionTypes.SEARCH_FAILED,
	payload: err
});
export const clearSearch = () => ({
	type: ActionTypes.SEARCH_CLEAR_RESULTS
});

//Get Popular tags
//http://lahuritv.net/api/tags/popular/20

export const fetchTags = () => (dispatch) => {
	dispatch(tagsLoading);
	return fetch(baseUrl + 'tags/popular/20')
		.then(
			(response) => {
				if (response.ok) {
					return response;
				} else {
					var error = new Error('Error ' + response.status + ' : ' + response.statusText);
					error.message = response;
					throw error;
				}
			},
			(err) => {
				var errMess = new Error(err.message);
				throw errMess;
			}
		)
		.then((response) => response.json())
		.then((tags) => dispatch(getTags(tags)))
		.catch((error) => dispatch(tagsFailed(error.message)));
};
export const tagsLoading = () => ({
	type: ActionTypes.TAGS_FAILED
});
export const getTags = (tags) => ({
	type: ActionTypes.GET_TAGS,
	payload: tags
});
export const tagsFailed = (err) => ({
	type: ActionTypes.TAGS_FAILED,
	payload: err
});

//increase View
export const increaseView = (id) => (dispatch) => {
	const postId = JSON.stringify(id);
	return fetch(baseUrl + `post/${postId}/views/increase`, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		credentials: 'same-origin'
	})
		.then(
			(response) => {
				if (response.ok) {
					console.log('Succcfully increased view');
				} else {
					var error = new Error('Error ' + response.status + ' : ' + response.statusText);
					error.message = response;
					throw error;
				}
			},
			(err) => {
				var errMess = new Error(error.message);
				throw errMess;
			}
		)
		.then((response) => console.log(response))
		.catch((error) => dispatch(increaseViewFailed(error.message)));
};

export const increaseViewFailed = (err) => ({
	type: ActionTypes.INCREASE_VIEW,
	payload: err
});
